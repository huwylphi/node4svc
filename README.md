# node4SVC

PoC Node.js app that consumes the SAUTER Vision Center REST API by publishing / subscribing data.

## Demo
![](example/nodejs-svc-cov-test.gif)

![backend log](example/nodejs console.png)
