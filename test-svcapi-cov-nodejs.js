// Test of COV methods from SVC API with nodejs (by Philippe Huwyler)
// BACnet Device <--> BACnet/IP <--> SVC (API) <--> http web-socket (signalR) <--> nodejs <--> http web-socket (socket.io) <--> web-client

'use strict';

// PARAMS
const SVCAPIURL              = 'http://192.168.10.100/VisionCenterApiService'   // svc api url
const SVCAPIUSER             = 'apiuser'        // svc api user name
const SVCAPIPWD              = 'apipassword'        // svc api user password
const AUTO_DICONNECT_TIMEOUT = 0                // auto disconnect SVC API web-socket in seconds. 0 = infinite
const WEBSRVPORT             = 3000             // web-server 
const DEBUG                  = 2                // 0 = no debug, 1 = debug level 1, 2 = debug level 2
const ANALOG_DOID            = 11082            // an analog datapoint id from svc. find id over DataObjectList svc api method
const BINARY_DOID            = 0                // an binary datapoint id from svc. find id over DataObjectList svc api method

// REQUIRES
const request = require('request-promise')      // for http requests between nodejs and SVC API
const signalR = require('signalr-client')       // for web-socket between nodejs and SVC API
const app     = require('express')()            // for web-client
const http    = require('http').Server(app)     // for web-server
const io      = require('socket.io')(http)      // for web-socket between web-client and web-server (nodejs)

// WEB-SRV
app.get('/',              (req, res) => { res.sendFile(__dirname + '/index.html') })
app.get('/lib/jquery.js', (req, res) => { res.sendFile(__dirname + '/lib/jquery-1.11.1.js') })
http.listen(WEBSRVPORT, () => { console.log('http listening on *:' + WEBSRVPORT) })

// ajax is a function to generate SVC API requests
function ajax(controller, type, data) {

    // set request parameters
    let options = {
        method: type,
        uri: SVCAPIURL + "/api/" + controller,
        json: true,  // automatically stringifies the body to JSON 
        jar: true,   // copy header session cookies
        resolveWithFullResponse: true
    }

    // individual parameters for GET or POST requests
    if (type === "POST") {
        options.form = data
    } else if (type === "GET") {
        options.qs = data
        options.forever = true
        options.followRedirect = false
    }

    // make the request and return the promise object
    return request(options)
}

// init initial values
let values = []
values[1] = 0
values[ANALOG_DOID] = 0
values[BINARY_DOID] = 0

// manage toggle event from web-clients
let onSocketIOSetValue = function(data) {
    console.log('message "SetValue" received: ' + data)

    let values = this
    ajax('DataObject/SetValue', 'POST', {'ObjectId': data.doid, 'PropertyId': 85, 'NewValue': data.NewValue, 'Priority' : 8 })
    .then((setValueRes) => {
        console.log("SVC API set value succeeded")
    })
    .catch((err) => {
        console.log("SVC API set value failed, error=" + err)
    })
}

// manage cov event from svc
let onSignalRcov = function(data) {
    console.log("svc cov: " + data.bacnetProperties[0].value)

    let values = this
    // save new value
    values[data.objectRef.doid] = data.bacnetProperties[0].value

    // send new cov to web-client via socket.io
    io.emit("cov", {"doid" : data.objectRef.doid, "data" : data})
}

// WEB-SOCKET from web-clients
io.on('connection', (socket) => {
    console.log('a web-client user connected')
    
    // manage toggle message from web-clients
    socket.on('SetValue', onSocketIOSetValue.bind(values))
})

// SVC API => post LOGIN request
ajax('Login', 'POST', { 'login': SVCAPIUSER, 'password': SVCAPIPWD })
.then((resLogin) => {
    // login feedback
    if(resLogin.body === "false") { console.log("SVC API authentication failed"); process.exit(1) }
    else { if(DEBUG > 0) { console.log("SVC API authentication succeeded") }}

    // login succeeded, get data object list
    ajax("DataObjectList", "GET", { pageNumber: 1, itemsPerPage: 100 })
    .then((resDataObjectList) => {
        //todo: parse array resDataObjectList.body.Objects[]...
        if(DEBUG > 1) { console.log("DataObjectList: %j", resDataObjectList.body.Objects) }
        
        //send DataObjectList to web-client
        io.emit("DataObjectList", resDataObjectList.body.Objects)

        // get data object list succeeded, build web-socket = signalR client => http://signalr.net/
        let signalRclient = new signalR.client(
                                SVCAPIURL + '/signalr',
                                ['subscriptionModuleHub'],  // SVC API COV subscription
                                10,                         // reconnection Timeout is optional and defaulted to 10 seconds
                                true)                       // true = start signalR web-socket client manually

        // copy "set-cookie" to "client.header.cookie" for future requests
        signalRclient.headers.cookie = resLogin.headers['set-cookie'].join(";")

        // get SVC API web-socket messages for debuging
        signalRclient.serviceHandlers = {
            bound:           ()        => { if(DEBUG > 0) { console.log("Websocket bound") }},
            messageReceived: (message) => { if(DEBUG > 1) { console.log("Websocket messageReceived: ", message) }; return false; },
            connectFailed:   (error)   => { if(DEBUG > 0) { console.log("Websocket connectFailed: ", error) } process.exit(1)},
            onerror:         (error)   => { if(DEBUG > 0) { console.log("Websocket onerror: ", error) } process.exit(1)},
            bindingError:    (error)   => { if(DEBUG > 0) { console.log("Websocket bindingError: ", error) } process.exit(1)},
            connectionLost:  (error)   => { if(DEBUG > 0) { console.log("Connection lost: ", error) }},
            reconnecting:    (retry)   => { if(DEBUG > 0) { console.log("Websocket retrying: ", retry) }; return true; },
            onUnauthorized:  (res)     => { if(DEBUG > 0) { console.log("onUnauthorized") } process.exit(1)},
            disconnected:    ()        => { if(DEBUG > 0) { console.log("Websocket disconnected") }},
            connected:       (connection) => {
                // web-socket connected
                if(DEBUG > 0) { console.log("Websocket connected") }

                // subscribe on cov for SVC system-time
                signalRclient.call(
                    'subscriptionModuleHub',        // Hub Name (case insensitive)
                    'registerProperties',           // Method Name (case insensitive)
                    [{                              // additional parameters to match signature
                        "objectRef": { doid: 1, vid: 0, did: 0, oid: 0, cid: 0 },   //doid from DataObjectList. 1 = svc system time
                        "bacnetProperties": [85],   // property id. 85 = present value
                        "bacnetPropertiesRendered": [],
                        "visualizationProperties": []
                    }])
                    .done((err, result) => {
                        if (err) { if(DEBUG > 0) { console.log("subscribe SVC COV failed: ", err) } process.exit(1)}
                    })

                // subscribe on cov for a SVC analog value doid 11082
                signalRclient.call('subscriptionModuleHub', 'registerProperties',
                    [{
                        "objectRef": { doid: ANALOG_DOID, vid: 0, did: 0, oid: 0, cid: 0 },
                        "bacnetProperties": [85],
                        "bacnetPropertiesRendered": [], "visualizationProperties": []
                    }])
                    .done((err, result) => {
                        if (err) { if(DEBUG > 0) { console.log("subscribe SVC COV failed: ", err) } process.exit(1)}
                    })

                // todo: subscribe on cov for a SVC binary value doid ???
                signalRclient.call('subscriptionModuleHub', 'registerProperties',
                    [{
                        "objectRef": { doid: BINARY_DOID, vid: 0, did: 0, oid: 0, cid: 0 },
                        "bacnetProperties": [85],
                        "bacnetPropertiesRendered": [], "visualizationProperties": []
                    }])
                    .done((err, result) => {
                        if (err) { if(DEBUG > 0) { console.log("subscribe SVC COV failed: ", err) } process.exit(1)}
                    })
            }
        }

        // manage svc cov
        signalRclient.on(
            'subscriptionModuleHub',	// Hub Name (case insensitive)
            'UpdateProperties',	        // Method Name (case insensitive)
            onSignalRcov.bind(values))  // Callback function with parameters matching call from hub)

        // explicitly disconnect after Xs
        if(AUTO_DICONNECT_TIMEOUT > 0) {
            setTimeout(() => { signalRclient.end(); process.exit(0)}, AUTO_DICONNECT_TIMEOUT * 1000)
        }

        // SVC API => manually start web-socket connection (signalR client)
        signalRclient.start()
    })
    .catch((err) => {
        console.log("SVC API get data object list failed, error=" + err); process.exit(1)
    })
})
.catch((err) => {
    console.log("SVC API authentication failed, error=" + err); process.exit(1)
})
